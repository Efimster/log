use crate::structs::log_message::LogMessage;
use crate::structs::logger::{ILogger};
use std::io;

#[cfg(test)]
use date::date_time::DateTime;

pub type LoggerFactory = Box<dyn Fn(&'static str) -> Box<dyn ILogger>>;

pub fn log_multi<F>(loggers:&mut [&mut dyn ILogger], f:F) -> Result<(), io::Error>
    where F:Fn()-> LogMessage
{
    let log_message = f();
    for logger in loggers.iter_mut() {
        logger.logger_impl(&log_message, false, true)?;
    }

    Ok(())
}

#[cfg(test)]
pub fn date_sub_seconds(date:DateTime, seconds:u64) -> DateTime {
    DateTime::from_seconds_since_unix_epoch(date.to_seconds_from_unix_epoch() - seconds)
}

#[cfg(test)]
mod test {
    use super::*;
    use date::constants::{SECONDS_IN_MINUTE, SECONDS_IN_HOUR};
    use crate::enums::log_level::LogLevel;
    use crate::enums::log_time_prefix::LogTimePrefix;
    use crate::structs::logger::Logger;

    #[test]
    fn test_log_multi() -> Result<(), io::Error>{
        let mut output = Vec::new();
        let mut output2 = Vec::new();
        {
            let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::Instant, "");
            logger.created = date_sub_seconds(logger.created, 23 * SECONDS_IN_HOUR + 59 * SECONDS_IN_MINUTE + 59);
            let mut logger: Box<dyn ILogger> = Box::new(logger);


            let mut logger2 = Logger::new(LogLevel::Info, &mut output2, LogTimePrefix::Instant, "");
            logger2.created = date_sub_seconds(logger2.created, 24 * SECONDS_IN_HOUR);
            let mut logger2: Box<dyn ILogger> = Box::new(logger2);

            log_multi(&mut [logger.as_mut(), logger2.as_mut()], || LogMessage::info("info"))?;
        }

        assert_eq!(output, b"23:59:59 [Info] info");
        assert_eq!(output2, b"00:00:00+1d [Info] info");

        Ok(())
    }
}