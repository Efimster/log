#[derive(PartialOrd, PartialEq, Debug, Copy, Clone)]
pub enum LogLevel {
    Error,
    Warning,
    Info,
    Detail,
    Detail2
}