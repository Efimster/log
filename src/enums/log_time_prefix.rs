#[derive(Copy, Clone)]
pub enum LogTimePrefix {
    None,
    Instant,
    TodayTime,
    Date,
    DateTime,
}