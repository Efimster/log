use crate::enums::log_level::LogLevel;

pub struct LogMessage {
    pub error:Option<String>,
    pub warning:Option<String>,
    pub info:Option<String>,
    pub detail:Option<String>,
    pub detail2:Option<String>,
}

impl LogMessage {
    fn new_empty() -> Self {
        LogMessage {
            error:None, warning:None, info:None, detail:None, detail2:None
        }
    }
    pub fn error(message:impl ToString) -> LogMessage {
        let mut log_message = LogMessage::new_empty();
        log_message.error = Some(message.to_string());
        log_message
    }

    pub fn warning(message:impl ToString) -> LogMessage {
        let mut log_message = LogMessage::new_empty();
        log_message.warning = Some(message.to_string());
        log_message
    }

    pub fn info(message:impl ToString) -> LogMessage {
        let mut log_message = LogMessage::new_empty();
        log_message.info = Some(message.to_string());
        log_message
    }

    pub fn detail(message:impl ToString) -> LogMessage {
        let mut log_message = LogMessage::new_empty();
        log_message.detail = Some(message.to_string());
        log_message
    }

    pub fn detail2(message:impl ToString) -> LogMessage {
        let mut log_message = LogMessage::new_empty();
        log_message.detail2 = Some(message.to_string());
        log_message
    }

    pub fn add(mut self, message:impl ToString, level:LogLevel) -> Self {
        match level {
            LogLevel::Error => self.error = Some(message.to_string()),
            LogLevel::Warning => self.warning = Some(message.to_string()),
            LogLevel::Info => self.info = Some(message.to_string()),
            LogLevel::Detail => self.detail = Some(message.to_string()),
            LogLevel::Detail2 => self.detail2 = Some(message.to_string()),
        }
        self
    }
}