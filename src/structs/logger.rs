use crate::enums::log_level::LogLevel;
use std::error::Error;
use std::io::{Write, stdout, stderr, Stdout, Stderr};
use std::io;
use crate::structs::log_message::LogMessage;
use crate::enums::log_time_prefix::LogTimePrefix;
use date::time::Time;
use date::date_time::DateTime;
use slib::traits::try_clone::TryClone;
use date::constants::SECONDS_IN_DAY;
use date::date::Date;

pub struct Logger<W:Write> {
    pub level: LogLevel,
    pub output: W,
    pub time_prefix: LogTimePrefix,
    pub subject_prefix:&'static str,
    pub(crate) created: DateTime,
}

impl Logger<Stderr> {
    pub fn stderr(level:LogLevel, time_prefix: LogTimePrefix, subject_prefix:&'static str) -> Logger<Stderr> {
        Logger::new(level, stderr(), time_prefix, subject_prefix)
    }
}

impl Logger<Stdout> {
    pub fn stdout(level:LogLevel, time_prefix: LogTimePrefix, subject_prefix:&'static str) -> Logger<Stdout> {
        Logger::new(level, stdout(), time_prefix, subject_prefix)
    }
}

impl<W:Write> Logger<W> {
    pub fn new(level:LogLevel, output:W, time_prefix: LogTimePrefix, subject_prefix:&'static str) -> Logger<W> {
        Logger {level, output, time_prefix, created: DateTime::now(), subject_prefix}
    }

    fn create_output(&mut self, string:&str, level:LogLevel, prefixes:bool) -> Result<Vec<u8>, io::Error> {
        let mut output = Vec::new();
        if prefixes {
            let _ = match self.time_prefix {
                LogTimePrefix::Instant => {
                    let mut seconds = self.seconds_elapsed();
                    let days = seconds / SECONDS_IN_DAY;
                    seconds = seconds % SECONDS_IN_DAY;
                    let time = Time::from_seconds(seconds);
                    if days == 0 {
                        output.write(format!("{} ", time).as_bytes())?
                    }
                    else {
                        output.write(format!("{}+{}d ", time, days).as_bytes())?
                    }
                },
                LogTimePrefix::TodayTime
                    => output.write(format!("{} ", DateTime::now().time).as_bytes())?,
                LogTimePrefix::Date
                    => output.write(format!("{} ", Date::today()).as_bytes())?,
                LogTimePrefix::DateTime
                    => output.write(format!("{} ", DateTime::now()).as_bytes())?,
                LogTimePrefix::None => 0,
            };
            output.write(format!("[{:?}] ", level).as_bytes())?;
            if self.subject_prefix.len() > 0 {
                output.write(format!("[{}] ", self.subject_prefix).as_bytes())?;
            }
        }
        output.write(string.as_bytes())?;
        Ok(output)
    }

    fn log_str_inner(&mut self, string:&str, level:LogLevel, new_line:bool, prefixes:bool) -> Result<(), io::Error> {
        let mut output = self.create_output(string, level, prefixes)?;
        if new_line {
            output.write(b"\n")?;
        }
        self.write_output(&output)
    }

    pub fn write_output(&mut self, output:&[u8]) -> Result<(), io::Error> {
        self.output.write(output)?;
        self.output.flush()
    }

    pub fn log_direct(&mut self, string:&str) -> Result<(), io::Error> {
        self.write_output(string.as_bytes())
    }

    pub fn flush(&mut self) -> Result<(), io::Error>{
        self.output.flush()
    }

    fn seconds_elapsed(&self) -> u64 {
        DateTime::now().to_seconds_from_unix_epoch() - self.created.to_seconds_from_unix_epoch()
    }

    pub fn into_output(self) -> W {
        self.output
    }
}

pub trait ILogger {
    fn logger_impl(&mut self, log_message:&LogMessage, new_line:bool, prefixes:bool) -> Result<(), io::Error>;

    fn log(&mut self, log_message:LogMessage) -> Result<(), io::Error> {
        self.logger_impl(&log_message, false, true)
    }

    fn log_f(&mut self, f:&dyn (Fn() -> LogMessage)) -> Result<(), io::Error> {
        let log_message = f();
        self.logger_impl(&log_message, false, true)
    }

    fn log_ln(&mut self, log_message:LogMessage) -> Result<(), io::Error> {
        self.logger_impl(&log_message, true, true)
    }

    fn log_ln_f(&mut self, f:&dyn (Fn() -> LogMessage)) -> Result<(), io::Error> {
        let log_message = f();
        self.logger_impl(&log_message, true, true)
    }

    fn log_no_prefix(&mut self, log_message:LogMessage) -> Result<(), io::Error> {
        self.logger_impl(&log_message, false, false)
    }

    fn log_no_prefix_f(&mut self, f:&dyn (Fn() -> LogMessage)) -> Result<(), io::Error> {
        let log_message = f();
        self.logger_impl(&log_message, false, false)
    }

    fn log_ln_no_prefix(&mut self, log_message:LogMessage) -> Result<(), io::Error> {
        self.logger_impl(&log_message, true, false)
    }

    fn log_ln_no_prefix_f(&mut self, f:&dyn (Fn() -> LogMessage)) -> Result<(), io::Error> {
        let log_message = f();
        self.logger_impl(&log_message, true, false)
    }

    fn info(&mut self, message:&str) {
        self.log(LogMessage::info(message)).expect("log error")
    }

    fn err(&mut self, message:&str) {
        self.log(LogMessage::info(message)).expect("log error")
    }
}

impl<W:Write> ILogger for Logger<W>{
    fn logger_impl(&mut self, log_message:&LogMessage, new_line:bool, prefixes:bool) -> Result<(), io::Error>
    {
        if let Some(detail2) = &log_message.detail2 {
            if self.level > LogLevel::Detail {
                return self.log_str_inner(&detail2, LogLevel::Detail2, new_line, prefixes);
            }
        }
        else if let Some(detail) = &log_message.detail {
            if self.level > LogLevel::Info {
                return self.log_str_inner(&detail, LogLevel::Detail, new_line, prefixes);
            }
        }
        if let Some(info) = &log_message.info {
            if self.level > LogLevel::Warning {
                return self.log_str_inner(&info, LogLevel::Info, new_line, prefixes);
            }
        }
        if let Some(warning) = &log_message.warning {
            if self.level > LogLevel::Error {
                return self.log_str_inner(&warning, LogLevel::Warning, new_line, prefixes);
            }
        }
        if let Some(error) = &log_message.error {
            return self.log_str_inner(&error, LogLevel::Error, new_line, prefixes);
        }

        Ok(())
    }
}

impl<W> TryClone for Logger<W>
    where W:Write + TryClone
{
    // type Error = LogError;

    fn try_clone(&self) -> Result<Self, Box<dyn Error>>
    {
        let output = self.output.try_clone()?;
        let mut logger = Logger::new(self.level, output, self.time_prefix, self.subject_prefix);
        logger.created = self.created;
        Ok(logger)
    }
}



#[cfg(test)]
mod test {
    use super::*;
    use date::date::Date;
    use date::constants::{SECONDS_IN_MINUTE, SECONDS_IN_HOUR};
    use crate::utils::log_util::date_sub_seconds;

    #[test]
    fn test_log() -> Result<(), io::Error>{
        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Detail, &mut output, LogTimePrefix::None, "");
        logger.log_f(&||LogMessage::info(format!("{}", "info")))?;
        assert_eq!(output, b"[Info] info");

        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::None, "");
        logger.log(LogMessage::detail(format!("{}", "detail")))?;
        assert_eq!(output, b"");

        Ok(())
    }

    #[test]
    fn test_log_prefix() -> Result<(), io::Error>{
        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::Date, "");
        let s = "hello log";
        logger.log(LogMessage::info(s))?;
        assert_eq!(output, format!("{} [Info] hello log", Date::today()).as_bytes());

        Ok(())
    }

    #[test]
    fn test_log_multiple_levels() -> Result<(), io::Error>{
        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::None, "");
        logger.log(LogMessage::info("info").add("detail", LogLevel::Detail))?;
        assert_eq!(output, b"[Info] info");

        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Detail, &mut output, LogTimePrefix::None, "");
        logger.log(LogMessage::info("info").add("detail", LogLevel::Detail))?;
        assert_eq!(output, b"[Detail] detail");

        Ok(())
    }

    #[test]
    fn test_log_instant_prefix() -> Result<(), io::Error>{
        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::Instant, "");
        logger.created = date_sub_seconds(logger.created, 23 * SECONDS_IN_HOUR + 59 * SECONDS_IN_MINUTE + 59);
        logger.log(LogMessage::info("info"))?;
        assert_eq!(output, b"23:59:59 [Info] info");

        let mut output = Vec::new();
        let mut logger = Logger::new(LogLevel::Info, &mut output, LogTimePrefix::Instant, "");
        logger.created = date_sub_seconds(logger.created, 24 * SECONDS_IN_HOUR);
        logger.log(LogMessage::info("info"))?;
        assert_eq!(output, b"00:00:00+1d [Info] info");

        Ok(())
    }

    #[cfg(feature = "unit-tests")]
    #[test]
    fn test_info() -> Result<(), io::Error>{
        use slib::structures::test_writer::TestWriter;

        let output = TestWriter::new();
        {
            let mut logger = Logger::new(LogLevel::Detail, output.clone(), LogTimePrefix::None, "");
            logger.info("Hello");
        }
        assert_eq!(&output.into_inner(), b"[Info] Hello");
        Ok(())
    }
}
